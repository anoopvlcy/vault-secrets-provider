/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"regexp"
	"strings"
)

const (
	// See:https://www.vaultproject.io/api-docs/index#authentication
	// nolint
	ClientTokenHeader = "X-Vault-Token"
	// See: https://www.vaultproject.io/api-docs#namespaces
	NamespaceHeader = "X-Vault-Namespace"
)

/**
 * See: https://www.vaultproject.io/api-docs/index#error-response
 */
type ErrorResponse struct {
	Errors []string `json:"errors"`
}

func (er ErrorResponse) Error() string {
	normErrors := make([]string, 0)
	r, _ := regexp.Compile(`^(\d+) errors? occurred:`)
	for i := 0; i < len(er.Errors); i++ {
		// nolint
		if match := r.MatchString(er.Errors[i]); match {
			// this is a multiline error: extract and normalize each line starting with "* "
			lines := strings.Split(er.Errors[i], "\n")
			for ln := 0; ln < len(lines); ln++ {
				line := strings.Trim(lines[ln], " \t")
				if strings.HasPrefix(line, "*") {
					normErrors = append(normErrors, line[2:])
				}
			}
		} else {
			// this is not a multiline: simply add
			normErrors = append(normErrors, er.Errors[i])
		}
	}
	er.Errors = normErrors
	return strings.Join(er.Errors, ", ")
}

/**
 * See: https://www.vaultproject.io/api-docs/auth/approle#login-with-approle
 */
type AppRoleLoginBody struct {
	RoleId   string `json:"role_id"`
	SecretId string `json:"secret_id"`
}

/**
 * See: https://www.vaultproject.io/api-docs/auth/jwt#jwt-login
 */
type JwtLoginBody struct {
	Role  string `json:"role,omitempty"`
	Token string `json:"jwt"`
}

type Authentication struct {
	ClientToken   string                 `json:"client_token"`
	Accessor      string                 `json:"accessor"`
	Policies      []string               `json:"policies"`
	TokenPolicies []string               `json:"token_policies"`
	Metadata      map[string]interface{} `json:"metadata"`
	LeaseDuration int64                  `json:"lease_duration"`
	EntityId      string                 `json:"entity_id"`
	TokenType     string                 `json:"token_type"`
	Renewable     bool                   `json:"renewable"`
	Orphan        bool                   `json:"orphan"`
}

type LoginResponse struct {
	Metadata      map[string]interface{} `json:"metadata"`
	WrapInfo      map[string]string      `json:"wrap_info"`
	LeaseDuration int64                  `json:"lease_duration"`
	LeaseId       string                 `json:"lease_id"`
	RequestId     string                 `json:"request_id"`
	Warnings      []string               `json:"warnings"`
	Renewable     bool                   `json:"renewable"`
	Auth          *Authentication        `json:"auth"`
}

/**
 * See: https://www.vaultproject.io/api-docs/secret/kv/kv-v1#read-secret
 */
type GetSecretResponse struct {
	Metadata      map[string]interface{} `json:"metadata"`
	WrapInfo      map[string]string      `json:"wrap_info"`
	LeaseDuration int64                  `json:"lease_duration"`
	LeaseId       string                 `json:"lease_id"`
	RequestId     string                 `json:"request_id"`
	Warnings      []string               `json:"warnings"`
	Renewable     bool                   `json:"renewable"`
	Data          map[string]interface{} `json:"data"`
}

type UpdateSecretResponse struct {
	WrapInfo      map[string]string      `json:"wrap_info"`
	LeaseDuration int64                  `json:"lease_duration"`
	LeaseId       string                 `json:"lease_id"`
	RequestId     string                 `json:"request_id"`
	Warnings      []string               `json:"warnings"`
	Renewable     bool                   `json:"renewable"`
	Data          map[string]interface{} `json:"data"`
}
